var gameEnum = {
    NONE : 0,
    LETTERS : 1,
    NUMBERS : 2,
    SOUNDS : 3
};

var activeGame = gameEnum.NONE;

var panelIndex = 1;

function mainStartup()
{
    //Setup Initial Screen
    hideQuickNavigationButtons();
    //showHome();
    
    // Register Menu click events
    $("#rightDiv").click(function() {
        showLettersFromHome();});
    
    $("#topDiv").click(function() {
        showNumbersFromHome();});
    

    $("#homeButton").click(function() {
        switch(activeGame)
        {
            case gameEnum.LETTERS:
                showHomeFromLetters();
                break;
            case gameEnum.NUMBERS:
                showHomeFromNumbers();
                break;
                
        }
        panelIndex = 1;
    }
    );

    $('#rightButton').click(function () {
        if (panelIndex < $('#lettersDivContainer div').length) {
            $("#lettersDiv" + panelIndex).animate({
                left: '-100%'
            }, 400, function () {
                /*var $this = $("#lettersDiv" + panelIndex);
                $this.css('left', '100%')
                    .appendTo($('.lettersDivContainer'));*/
            });
            $("#lettersDiv" + panelIndex).next().animate({
                left: '0px'
            }, 400);
            panelIndex++;
        }
    });

    $('#leftButton').click(function () {

        if (panelIndex > 1) {
            $("#lettersDiv" + panelIndex).animate({
                left: '100%'
            }, 400, function () {
                /*var $this = $("#lettersDiv" + panelIndex);
                $this.css('right', '-150%')
                    .appendTo($('.lettersDivContainer'));*/
            });
            $("#lettersDiv" + panelIndex).prev().animate({
                left: '0px'
            }, 400);
            panelIndex--;
        }
    });    
    
}




function hideQuickNavigationButtons()
{
    $("#navigationDiv").hide();
}


function hideNavigationButtons()
{
    $("#navigationDiv").fadeOut(400);
}


function showNavigationButtons()
{
    $("#navigationDiv").fadeIn(400);
}


function resetPanelPositions()
{
    $("#homeDiv").css("left", "0px");
    $("#lettersDivContainer").css("left", "100%");
}


function showHomeFromLetters()
{
    hideNavigationButtons();
    
    $('#homeDiv').animate({left: "0px"}, 400, "linear");
    $('#lettersDivContainer').animate({left: "100%", queue: "false"}, 400, "linear", function(){});
    activeGame = gameEnum.NONE;
}


function showLettersFromHome()
{
    panelIndex = 1;
    $("#lettersDiv" + panelIndex).css('left', '0px');
    
    $('#homeDiv').animate({left: "-100%"}, 400, "linear");
    $('#lettersDivContainer').animate({left: "0px", queue: "false"}, 400, "linear", function(){showNavigationButtons();});
    activeGame = gameEnum.LETTERS;
    
}


function showNumbersFromHome()
{
    $('#homeDiv').animate({top: "100%"}, 400, "linear");
    $('#numbersDivContainer').animate({top: "0px", queue: "false"}, 400, "linear", function(){showNavigationButtons();});
    activeGame = gameEnum.NUMBERS;
}

function showHomeFromNumbers()
{
    hideNavigationButtons();
    $('#homeDiv').animate({top: "0px"}, 400, "linear");
    $('#numbersDivContainer').animate({top: "-100%", queue: "false"}, 400, "linear", function(){});
    activeGame = gameEnum.NONE;
}

